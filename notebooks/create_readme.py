import os

for dirpath, dirnames, filenames in os.walk('.'):
    if '.git' in dirnames:
        dirnames.remove('.git')  # exclude .git directory
    readme_path = os.path.join(dirpath, 'README.md')
    if not os.path.exists(readme_path):  # don't overwrite existing README.md
        with open(readme_path, 'w') as f:
            f.write("""
                _  _  __  ____  ____   __      __   __  
                ( \/ )(  )(_  _)(  _ \ / _\    / _\ (  ) 
                / \/ \ )(   )(   )   //    \  /    \ )(  
                \_)(_/(__) (__) (__\_)\_/\_/  \_/\_/(__) 

                # MitraAI Modern AI Course matarial.
""")
            f.write('# README\n\nThis is a placeholder README.md for ' + dirpath)