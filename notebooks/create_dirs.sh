#!/bin/bash

# An array of directory names for your modules
declare -a modules=(
    "01_Foundations_of_Machine_Learning_and_Deep_Learning"
    "02_Processing_and_Analyzing_Text_and_Numeric_Data"
    "03_Image_and_Video_Processing"
    "04_Voice_Recognition_and_Language_Processing"
    "05_Introduction_to_Large_Language_Models"
    "06_Fine_tuning_LLMs_for_Custom_Applications"
    "07_Implementing_LLMs_in_Enterprise_Setting"
    "08_Deep_Dive_into_Data_Science"
    "09_Introduction_to_Generative_AI"
    "10_Advanced_Generative_AI"
    "11_Capstone_Project"
)

# Loop through the array and create directories
for module in "${modules[@]}"; do
    mkdir "$module"
done

echo "Directories created successfully."